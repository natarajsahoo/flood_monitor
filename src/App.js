import React, { Component } from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import './App.css';
import Dashboard from './Components/Dashboard.jsx';
import Forecast from './Components/Forecast.jsx';
import Device from './Components/Device.jsx';
import Map from './Components/Map.jsx';
import User from './Components/User.jsx';
import Station from './Components/Station.jsx';
import Alert from './Components/Alert.jsx';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <Route exact path="/" component={Dashboard}/>
            <Route exact path="/forecast" component={Forecast}/>
            <Route exact path="/device" component={Device}/>
            <Route exact path="/map" component={Map}/>
            <Route exact path="/user" component={User}/>
            <Route exact path="/station" component={Station}/>
            <Route exact path="/alert" component={Alert}/>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;